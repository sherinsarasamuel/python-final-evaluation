# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 11:10:53 2020

@author: user
"""

#superclass Pet      
class Pet:
    
    def __init__(self, species= None, name=""):
        if species=='cat' or species=='dog' or species=='horse' or species=='hamster' :
            self.species=species
            self.name=name;
        else:
            raise TypeError("Invalid Species")

#printing details based on name
    def __str__(self):
        if self.name=='':
            return "Species: "+self.species+". Unnamed "
        else:
            return "Species: "+self.species+". Named: "+ self.name

class Dog(Pet):
    #to initialize attributes
    def __init__(self,name,chases='Cats'):
        self.name=name
        Pet.__init__(self,'dog',self.name)
        self.chases=chases;
        
    def __str__(self):
        if self.name=='':
            
            return "Species: "+self.species+". Unnamed. Chases "+self.chases
        else:
            return "Species: "+self.species+". Named: "+self.name+". Chases "+self.chases

#Cat class subclass of Pet     
class Cat(Pet):
    
    def __init__(self,name='', hates='Dogs'):
        
        self.name=name
        self.hates=hates
        Pet.__init__(self,'cat',self.name)
    
    #to print details based on name
    def __str__(self):
        if self.name=='':
            return "Species: "+self.species+". Unnamed. Hates "+self.hates
        else:
            return "Species: "+self.species+". Named: "+self.name+". Hates "+self.hates


#defining main function
def main():
    #valid species
    Rocky=Pet('hamster','Rocky')
    print(Rocky)
    Fido=Dog('Fido','Rats')
    print(Fido)
    Felix=Cat('Felix')
    print(Felix)
    #invalid species in pets class
    Blackie=Pet('rat','Blackie')
    print('Blackie')
  
main()
