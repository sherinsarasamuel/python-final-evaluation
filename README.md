# Python Final Evaluation

Class “Pet” should include the following methods:


- __init__ () : Accepts three arguments: self, species(default None), and name(default “”). Stores species and name as data attributes. Raises Error, if the species is not one of the following : 'dog', 'cat', 'horse', 'hamster'.


- __str__() : Returns a string which depends on whether the pet is named or not.

If named: "Species of: xxx, named xyy", where xxx is the species data attribute and yyy is the name data attribute

otherwise: "Species of: xxx, unnamed", where xxx is the species data attribute



Class “Dog” (which is a subclass of class “Pet”) should include the following methods : 


- __init__() : Accepts three arguments: self, name(default “”), and chases(default“Cats”). 

Uses the constructor for class “Pet” to store species(“Dog”) and ‘name’ as data attributes. 

Stores ‘chases’ as a data attribute.


- __str__() : Returns a string which depends on whether the dog is named or not.

If named: "Species of: Dog, named yyy, chases zzz", where yyy is the name data attribute (as above) and zzz is the chases data attribute.

Otherwise: "Species of: Dog, unnamed,chases zzz", where zzz is the chases data attribute


Class “Cat” (which is a subclass of class “Pet”) should include the following methods: 


1. __init__ () : Accepts three arguments: self, name(default “”), and hates(default “Dogs”).

Uses the constructor for class “Pet” to store species(“Cat”) and ‘name’ as data attributes. 

Stores ‘hates’ as a data attribute.


2. __str__ () : Returns a string which depends on whether the cat is named or not.

If named: "Species of: Cat, named yyy, hateszzz", where yyy is the name data attribute(as

above) and zzz is the hates data attribute.

Otherwise: "Species of: Cat, unnamed, hateszzz", where zzz is the hates data attribute.


* Write a main() class to create objects of each of these classes and demonstrate their working.